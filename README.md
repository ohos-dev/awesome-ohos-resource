# awesome-ohos-resource

## 项目介绍

一个很棒的OpenHarmony综合资源列表

## 学习资料

- [OpenHarmony官方文档](https://docs.openharmony.cn/pages/v4.0/zh-cn/application-dev/application-dev-guide.md/)
- [applications_app_samples](https://gitee.com/openharmony/applications_app_samples) `应用示例大集合`
- [codelabs](https://gitee.com/openharmony/codelabs) `基于趣味场景的应用示例`

## 三方库相关

- [OpenHarmony三方库中心仓](https://ohpm.openharmony.cn/)

## 跨平台开发技术

- [ArkUI-X](https://gitee.com/arkui-x)
- [Flutter](https://gitee.com/openharmony-sig/flutter_flutter)
- [Qt](https://gitee.com/openharmony-sig/qt)
- [Taro](https://github.com/NervJS/taro/discussions/14766)

## 闭源应用

- [开心消消乐](https://www.openharmony.cn/certification/sampleApplication)
- [香橙派应用商店](https://gitee.com/openharmony-dg/device_board_orangepi/blob/OpenHarmony-4.0-Release/orangepi_5b/resources/hap/Appstore/appstore.hap)
- [万里红软件商店](https://gitee.com/develop-phone-open-source/applications_hap/blob/OpenHarmony-4.0-Release/AppStore.hap)
